#include "maglev_plate.h"
#include "tim.h"
#include "adc.h"
#include "usart.h"
#include <string.h>
#include <stdio.h>

#define LIMIT(x,min,max) (x)=(((x)<=(min))?(min):(((x)>=(max))?(max):(x)))

#define ADC_CHANNEL_NUM 3

#define BUFFER_LEN 100

uint16_t adc_value[ADC_CHANNEL_NUM];

uint8_t uart_tx_buffer[BUFFER_LEN];

PID pid_x = {0.18, 0, 11, 100, 2000, 0, 0, 0, 0};
PID pid_y = {0.18, 0, 11, 100, 2000, 0, 0, 0, 0};

int16_t target_x = 2048;
int16_t target_y = 2048;

void pid_calc(PID *pid, int16_t target, int16_t real)
{
	pid->last_error = pid -> error;
	pid->error = target - real;
	
	pid->output = (pid -> error - pid -> last_error) * pid -> Kd; // 微分
	pid->output += pid->error*pid->Kp; // 比例
	pid->integral += pid->error*pid->Ki; // 积分求和
	LIMIT(pid->integral,-pid->max_integral,pid->max_integral); // 积分限幅
	pid->output += pid->integral; // 积分
	
	LIMIT(pid->output,-pid->max_output,pid->max_output); // 输出限幅
}

void mp_init()
{
	HAL_TIM_Base_Start(&htim3);
	HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adc_value, ADC_CHANNEL_NUM);
	
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
	
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
	
	mp_set_output(0,0,0,0);
}

/**
* @brief 设置线圈输出
* @details 正为推，负为拉
*/
void mp_set_output(int16_t x1, int16_t x2, int16_t y1, int16_t y2)
{
	if(x1 > 0)
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, x1);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, 0);
	}else
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 0);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, -x1);
	}
	
	if(x2 > 0)
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, x2);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_4, 0);
	}else
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, 0);
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_4, -x2);
	}
	
	if(y1 > 0)
	{
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, y1);
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, 0);
	}else
	{
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 0);
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, -y1);
	}
	
	if(y2 > 0)
	{
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, y2);
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, 0);
	}else
	{
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, 0);
		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, -y2);
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if(adc_value[2] > 2048)
	{
		pid_calc(&pid_x, target_x, adc_value[1]);
		pid_calc(&pid_y, target_y, adc_value[0]);
	}else
	{
		pid_x.output = 0;
		pid_y.output = 0;
	}
	
	mp_set_output(pid_x.output,-pid_x.output,pid_y.output,-pid_y.output);
		
	sprintf((char *)uart_tx_buffer, "%d,%d,%d,%d,%d\n", adc_value[1], adc_value[0], adc_value[2],(int)pid_x.output,(int)pid_y.output);
	HAL_UART_Transmit_DMA(&huart1, uart_tx_buffer, strlen((const char *)uart_tx_buffer));
}