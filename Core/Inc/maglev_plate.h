#ifndef __MAGLEV_PLATE_H
#define __MAGLEV_PLATE_H

#include "main.h"

typedef struct
{
	// ���ò���
	float Kp, Ki, Kd, max_integral, max_output;
	
	// �洢����
	int16_t error, last_error;
	float integral, output;
}PID;

extern int16_t target_x;
extern int16_t target_y;

void mp_init();
void mp_set_output(int16_t x1, int16_t x2, int16_t y1, int16_t y2);

#endif /* __MAGLEV_PLATE_H */